import play.Project._
import sbt.Keys._
import sbt._

object ApplicationBuild extends Build {

  val appName = "ReliaCloud"
  val appVersion = "1.0-SNAPSHOT"

  val appDependencies = Seq(
    // Add your project dependencies here,
    javaCore, jdbc, javaJdbc,
    "org.mongodb.morphia" % "morphia" % "1.0.1",
    "org.mongodb" % "mongo-java-driver" % "2.10.1",
    "postgresql" % "postgresql" % "9.1-901-1.jdbc4",
    "ws.securesocial" %% "securesocial" % "2.1.4"
  )

  val main = play.Project(appName, appVersion, appDependencies).settings(
    // Add your own project dbsettings here
    resolvers += "Maven repository" at "http://morphia.googlecode.com/svn/mavenrepo/",
    resolvers += "MongoDb Java Driver Repository" at "http://repo1.maven.org/maven2/org/mongodb/mongo-java-driver/",
    resolvers += Resolver.sonatypeRepo("releases")
  )

}
