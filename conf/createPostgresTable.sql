CREATE TABLE public.state (
  id         BIGSERIAL             NOT NULL,
  sample     INTEGER               NOT NULL,
  sim_id     CHARACTER VARYING(24) NOT NULL,
  sim_res_id CHARACTER VARYING(24) NOT NULL,
  cpu_avail  BIGINT                NOT NULL,
  mem_avail  BIGINT                NOT NULL,
  hdd_avail  BIGINT                NOT NULL,
  ban_avail  BIGINT                NOT NULL,
  rel        DOUBLE PRECISION      NOT NULL,
  var_rel    DOUBLE PRECISION      NOT NULL
);
