import org.mongodb.morphia.Morphia;

import com.mongodb.MongoClient;

import controllers.MorphiaObject;
import models.Cloud;
import models.Cluster;
import models.Simulation;
import models.SimulationResult;
import models.User;
import models.VM;
import models.VMCount;
import models.VMGroup;
import models.VMProbabilityMap;
import play.GlobalSettings;
import play.Logger;

public class Global extends GlobalSettings {

  @Override
  public void onStart(play.Application arg0) {
    super.beforeStart(arg0);
    Logger.debug("** onStart **");
    MorphiaObject.morphia = new Morphia();
    MorphiaObject.ds =
        MorphiaObject.morphia.createDatastore(new MongoClient(), "test");

    MorphiaObject.morphia.map(Simulation.class);
    MorphiaObject.morphia.map(SimulationResult.class);
    MorphiaObject.morphia.map(Cloud.class);
    MorphiaObject.morphia.map(Cluster.class);
    MorphiaObject.morphia.map(VM.class);
    MorphiaObject.morphia.map(VMGroup.class);
    MorphiaObject.morphia.map(User.class);
    MorphiaObject.morphia.map(VMCount.class);
    MorphiaObject.morphia.map(VMProbabilityMap.class);

    MorphiaObject.ds.ensureIndexes();
    MorphiaObject.ds.ensureCaps();

    Logger.debug("** Morphia datastore: " + MorphiaObject.ds.getDB());
  }
}
