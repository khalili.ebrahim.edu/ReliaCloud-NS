package simulation.actors;

import java.util.ArrayList;
import java.util.List;

import akka.actor.UntypedActor;
import models.Simulation;
import models.SimulationResult;
import simulation.SimUtils;
import simulation.data.NotificationData;
import simulation.data.ResultData;

/**
 * Created with IntelliJ IDEA. User: Brett Date: 3/14/13 Time: 11:19 PM
 */
class SaveResultActor extends UntypedActor {
  private Simulation sim;
  private int numCompleted = 0;
  private final int numSims;
  private final long startTime;
  private double[] reliabilities;
  private List<Integer> histCounts; // holds the count of binned R values for
                                    // children SimResults

  public SaveResultActor(String simId, long startTime) {
    sim = Simulation.findById(simId);
    numSims = sim.getNumSims();
    this.startTime = startTime;
    reliabilities = new double[numSims];

    histCounts = new ArrayList<>(101);
    for (int i = 0; i < 101; i++)
      histCounts.add(0);
  }

  @Override
  public void preStart() {
    System.out.println("SaveResultActor started");
  }

  @Override
  public void postStop() {
    System.out.println("SaveResultActor killed");
  }

  @Override
  public void onReceive(Object message) throws Exception {
    if (message instanceof ResultData) {
      sender().tell(saveResources((ResultData) message), getSelf());
    } else {
      unhandled(message);
    }
  }

  private NotificationData saveResources(ResultData rd) {
    SimulationResult sr = rd.getSimulationResult();

    // calculate reliability
    double reliability = 1 - sr.getLOCP();
    reliabilities[numCompleted] = reliability;

    // find which bin this result falls in and update
    int bin = (int) Math.floor(reliability * 100);
    histCounts.set(bin, histCounts.get(bin) + 1);

    numCompleted++;

    if (numCompleted == numSims) {
      long duration = System.nanoTime() - startTime;
      double seconds = (duration / 1000000.0) / 1000.0;
      double runTime = (Double.valueOf(seconds));
      double avgRel = SimUtils.average(reliabilities);

      Simulation.addResult(sim, sr, runTime, avgRel,
          SimUtils.stddev(reliabilities, avgRel), histCounts);
    } else {
      Simulation.addResult(sim, sr);
    }

    return new NotificationData("notify");
  }
}
