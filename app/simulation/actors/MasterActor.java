package simulation.actors;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.Terminated;
import akka.actor.UntypedActor;
import akka.actor.UntypedActorFactory;
import akka.routing.RoundRobinRouter;
import akka.routing.SmallestMailboxRouter;
import simulation.data.AllocationData;
import simulation.data.NotificationData;
import simulation.data.ResultData;
import simulation.data.SimSetupData;

/**
 * Created with IntelliJ IDEA. User: Brett Date: 3/15/13 Time: 1:16 AM
 */
public class MasterActor extends UntypedActor {
  private final int numSims;
  private long startTime = System.nanoTime();
  private ActorRef simSetupActor;
  private ActorRef simulateActor;
  private ActorRef saveActor;
  private ActorRef notifyActor;

  @Override
  public void preStart() {
    System.out.println("MasterActor started.");
  }

  @Override
  public void postStop() {
    System.out.println("MasterActor killed.");
  }

  public MasterActor(final String simId, final int numSims) {
    this.numSims = numSims;

    simSetupActor = getContext().actorOf(
        new Props(SimSetupActor.class).withRouter(new RoundRobinRouter(3)),
        "setup");
    simulateActor = getContext().actorOf(
        new Props(SimulateActor.class).withRouter(new SmallestMailboxRouter(8)),
        "simulate");

    // single actor for saving individual results to DB
    saveActor = getContext().actorOf(new Props(new UntypedActorFactory() {
      public UntypedActor create() {
        return new SaveResultActor(simId, startTime);
      }
    }), "save");

    // single actor for notifying user of results being available
    notifyActor = getContext().actorOf(new Props(new UntypedActorFactory() {
      public UntypedActor create() {
        return new NotificationActor(numSims);
      }
    }), "notify");
  }

  @Override
  public void onReceive(Object message) throws Exception {
    if (message instanceof AllocationData) {
      for (int i = 0; i < numSims; i++) {
        simSetupActor.tell(message, getSelf());
      }
    } else if (message instanceof SimSetupData) {
      simulateActor.tell(message, getSelf());
    } else if (message instanceof ResultData) {
      saveActor.tell(message, getSelf());
    } else if (message instanceof NotificationData) {
      notifyActor.tell(message, getSelf());
    } else if (message instanceof String) {
      getContext().stop(getSelf());
    } else if (message instanceof Terminated) {
      System.out.println("Unhandled msg");
    } else {
      unhandled(message);
    }
  }
}
