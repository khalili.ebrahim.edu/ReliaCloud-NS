package simulation.actors;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

import akka.actor.UntypedActor;
import models.*;
import simulation.data.AllocationData;
import simulation.data.SimSetupData;


class SimSetupActor extends UntypedActor {
  @Override
  public void preStart() {
    System.out.println("SimSetupActor started");
  }

  @Override
  public void onReceive(Object message) throws Exception {
    if (message instanceof AllocationData) {
      AllocationData allocData = (AllocationData) message;

      if (allocData.getSim().getVmDistributionType().equals("Identical")) {
        sender().tell(setupIdenticalAllocation(allocData), getSelf());
      }
      else if(allocData.getSim().getVmDistributionType().equals("Distribution"))
        sender().tell(setupDistributionAlloc(allocData), getSelf());
    }
  }

  private SimSetupData setupIdenticalAllocation(AllocationData ad) {
    Simulation sim = ad.getSim();
    ArrayList<VMProbabilityMap> probs = sim.getProbabilityList();
    int numVMs = sim.getNumVMs();

    ArrayList<VM> vms = new ArrayList<>();
    ArrayList<VMCount> counts = new ArrayList<>(sim.getNumVMs());

    int pNeeded = 0;
    int mNeeded = 0;
    int hNeeded = 0;
    int bNeeded = 0;

    int totalCount = 0;
    double probability;

    for (VMProbabilityMap map : probs) {
      probability = map.getProbability();
      if (probability > 0.0) {
        int count = (int) Math.floor(probability * numVMs);
        totalCount += count;
        for (int j = 0; j < count; j++)
          vms.add(map.getVm());

        counts.add(new VMCount(map.getVm(), count));
      }
    }
    if (totalCount != numVMs) {
      int difference = numVMs - totalCount;
      VMCount last = counts.get(counts.size() - 1);
      counts.set(counts.size() - 1,
              new VMCount(last.getVm(), last.getCount() + 1));
      totalCount += difference;
    }

    for (VM vm : vms) {
      pNeeded += vm.getCores();
      mNeeded += vm.getMemory();
      hNeeded += vm.getHdd();
      bNeeded += vm.getBandwidth();
    }

    return new SimSetupData(ad.getSim(), vms, counts, pNeeded, mNeeded, hNeeded, bNeeded);
  }

  private SimSetupData setupDistributionAlloc(AllocationData ad) {
    String allocType = "Distribution";
    Simulation sim = ad.getSim();
    ArrayList<VMProbabilityMap> probs = sim.getProbabilityList();
    int numVMs = sim.getNumVMs();

    HashMap<VM, Integer> countMap = new HashMap<>();

    int pNeeded = 0;
    int mNeeded = 0;
    int hNeeded = 0;
    int bNeeded = 0;

    double sum;
    int count = 0;
    Random rand = ThreadLocalRandom.current();
    while (count < numVMs) {
      sum = 0;
      double r = rand.nextDouble();

      for (VMProbabilityMap map : probs) {
        sum += map.getProbability();

        if (r <= sum) {
          // keep track of count of each VM added
          if (!countMap.containsKey(map.getVm()))
            countMap.put(map.getVm(), 1);
          else
            countMap.put(map.getVm(), countMap.get(map.getVm()) + 1);
          count++;
          break;
        }
      }
    }

    ArrayList<VMCount> counts = new ArrayList<>(countMap.size());

    for (Map.Entry<VM, Integer> entry : countMap.entrySet()) {
      counts.add(new VMCount(entry.getKey(), entry.getValue()));

      pNeeded += entry.getKey().getCores() * entry.getValue();
      mNeeded += entry.getKey().getMemory() * entry.getValue();
      hNeeded += entry.getKey().getHdd() * entry.getValue();
      bNeeded += entry.getKey().getBandwidth() * entry.getValue();
    }

    return new SimSetupData(ad.getSim(), null, counts, pNeeded, mNeeded, hNeeded, bNeeded);
  }




}
