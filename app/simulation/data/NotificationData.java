package simulation.data;

/**
 * Created with IntelliJ IDEA. User: Brett Date: 3/15/13 Time: 10:51 AM
 */
public final class NotificationData {
  private final String simResId;

  public NotificationData(String simResId) {
    this.simResId = simResId;
  }

  public String getSimResId() {
    return simResId;
  }
}
