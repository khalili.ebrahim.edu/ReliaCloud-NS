package simulation.data;

import models.SimulationResult;

/**
 * Created with IntelliJ IDEA. User: Brett Date: 3/14/13 Time: 11:22 PM
 */
public final class ResultData {
  private final SimulationResult simulationResult;

  private final boolean saveResult;
  private final boolean saveResources;

  public ResultData(SimulationResult sr, boolean saveResult,
      boolean saveResources) {
    this.simulationResult = sr;

    this.saveResult = saveResult;
    this.saveResources = saveResources;
  }

  public SimulationResult getSimulationResult() {
    return simulationResult;
  }

  public boolean isSaveResult() {
    return saveResult;
  }

  public boolean isSaveResources() {
    return saveResources;
  }
}
