package simulation;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import models.Cluster;
import models.Simulation;
import models.VM;
import models.VMProbabilityMap;

/**
 * Created with IntelliJ IDEA. User: Brett Date: 3/12/13 Time: 11:47 PM
 */
public class SimUtils {

  private static final NumberFormat nf = new DecimalFormat("0.000000");
  private static final NumberFormat nf2 = new DecimalFormat("#,##0.00");
  private static final NumberFormat nf3 = new DecimalFormat("#,###");

  public static int calculateVMsForFail(Simulation s) {
    long cpuAvail = 0;
    long memAvail = 0;
    long hddAvail = 0;
    long bandAvail = 0;

    for (Cluster c : s.getCloud().getClusters()) {
      cpuAvail += (c.getCores() * c.getNodes());
      memAvail += (c.getMemory() * c.getNodes());
      hddAvail += (c.getHdd() * c.getNodes());
      bandAvail += (c.getBandwidth() * c.getNodes());
    }

    double cpuNeed = 0;
    double memNeed = 0;
    double hddNeed = 0;
    double bandNeed = 0;

    for (VMProbabilityMap vmpm : s.getProbabilityList()) {
      if (vmpm.getProbability() != 0) {
        VM curr = vmpm.getVm();

        cpuNeed += curr.getCores() * vmpm.getProbability();
        memNeed += curr.getMemory() * vmpm.getProbability();
        hddNeed += curr.getHdd() * vmpm.getProbability();
        bandNeed += curr.getBandwidth() * vmpm.getProbability();
      }
    }

    double numVMs = cpuAvail / cpuNeed;

    // find the limiting resource
    if (memAvail / memNeed < numVMs)
      numVMs = memAvail / memNeed;
    if (hddAvail / hddNeed < numVMs)
      numVMs = hddAvail / hddNeed;
    if (bandAvail / bandNeed < numVMs)
      numVMs = bandAvail / bandNeed;

    return (int) Math.ceil(numVMs * 1.2);
  }

  public static NumberFormat getNf() {
    return nf;
  }

  public static NumberFormat getNf2() {
    return nf2;
  }

  public static NumberFormat getNf3() {
    return nf3;
  }

  public static double average(double[] arr) {
    double sum = 0;
    for (double val : arr)
      sum += val;

    return sum / arr.length;
  }

  public static double stddev(double[] arr, double avg) {
    double sum = 0;
    for (double val : arr) {
      sum += Math.pow(val - avg, 2);
    }

    return Math.sqrt(sum / (arr.length - 1));
  }
}


