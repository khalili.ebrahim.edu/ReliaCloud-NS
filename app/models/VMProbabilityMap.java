package models;


import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Reference;

/**
 * Created with IntelliJ IDEA. User: Brett Date: 3/18/13 Time: 8:11 AM
 */

@Embedded
public class VMProbabilityMap {

  @Reference
  private VM vm;
  private double probability;

  public VMProbabilityMap() {}

  public VMProbabilityMap(VM vm, double probability) {
    this.vm = vm;
    this.probability = probability;
  }

  public VM getVm() {
    return vm;
  }

  public double getProbability() {
    return probability;
  }
}
