package models;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import Allocation.VmSchedualTimeShare;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Reference;
import org.mongodb.morphia.annotations.Transient;

import controllers.MorphiaObject;
import play.Logger;



@Entity
public class Cluster {
  @Transient
  // private final double[] afr = {0.02, 0.01, 0.08, 0.01};
  private final double[] afr =
          {0.00039992, 0.00009999500016666, 0.006379564, 0.00009999500016666};

  @Id
  private ObjectId id;
  private String name;
  private int cores;
  private int memory;
  private int hdd;
  private int bandwidth;
  private int nodes;
  private boolean isActive;
  private boolean isDefault; // specifies whether this is a global component that is available to all users

  @Reference
  private User user;

  @Reference
  private List<Cloud> clouds = new ArrayList<>();

  /********************* create other variables *****************************/
  private final ArrayList<VM> vmList = new ArrayList<VM>();  // vm list created in this cluster
  private VmSchedualTimeShare TS = new VmSchedualTimeShare(this);




  public static void create(Cluster c) {
    MorphiaObject.ds.save(c);
  }

  public static void delete(String idToDelete) {
    Cluster toDelete = MorphiaObject.ds.find(Cluster.class).field("_id")
            .equal(new ObjectId(idToDelete)).get();
    if (toDelete != null) {
      Logger.info("toDelete: " + toDelete);
      MorphiaObject.ds.delete(toDelete);
    } else {
      Logger.debug("Cluster id not Found: " + idToDelete);
    }
  }

  public static void update(Cluster c) {
    MorphiaObject.ds.save(c);
  }

  public static void markActive(String id) {
    Cluster c = findById(id);
    if (!c.isDefault && !c.isActive) {
      c.setActive(true);
      update(c);
    }
  }

  public static List<Cluster> findHidden() {
    return MorphiaObject.ds.find(Cluster.class).field("isActive").equal(false)
            .asList();
  }

  public static void markInactive(String id) {
    Cluster c = findById(id);

    if (!c.isDefault()) {
      c.setActive(false);
      update(c);
    }
  }

  public static Cluster findById(String id) {
    return MorphiaObject.ds.find(Cluster.class).field("_id")
            .equal(new ObjectId(id)).get();
  }

  public static List<Cluster> findActive() {
    return MorphiaObject.ds.find(Cluster.class).field("isActive").equal(true)
            .asList();
  }

  public final boolean[] getState() {
    Random rand = ThreadLocalRandom.current();
    double rP = rand.nextDouble();
    double rM = rand.nextDouble();
    double rH = rand.nextDouble();
    double rB = rand.nextDouble();

    // rP = -Math.log(1 - rP)/(0.02);
    // rM = -Math.log(1 - rM)/(0.01);
    // rH = -Math.log(1 - rH)/(0.08);
    // rB = -Math.log(1 - rB)/(0.01);

    // state of server -> maps to {P,M,H,B}
    return new boolean[] {rP >= afr[0], rM >= afr[1], rH >= afr[2],
            rB >= afr[3]};
  }

  public static boolean hasActiveCloudRef(Cluster clust) {
    List<Cloud> clouds = Cloud.findRefClouds(clust);

    boolean hasActiveRef = false;
    for (Cloud c : clouds) {
      if (c.isActive()) {
        hasActiveRef = true;
        break;
      }
    }

    return hasActiveRef;
  }



  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (o == null || getClass() != o.getClass())
      return false;

    Cluster cluster = (Cluster) o;

    return !(id != null ? !id.equals(cluster.id) : cluster.id != null);
  }

  @Override
  public int hashCode() {
    return id != null ? id.hashCode() : 0;
  }


  @Override
  public String toString() {
    return "Cluster{" + "idCluster=" + getId() + ", name='" + getName() + '\''
            + ", cores=" + getCores() + ", memory=" + getMemory() + ", hddSize="
            + getHdd() + ", bandwidth=" + getBandwidth() + ", nodes=" + getNodes()
            + '}';
  }


  /****************************** set and gets methods*****************************/

  public double[] getAfr() {
    return afr;
  }

  public ObjectId getId() {
    return id;
  }

  public void setId(ObjectId id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getCores() {
    return cores;
  }

  public void setCores(int cores) {
    this.cores = cores;
  }

  public int getMemory() {
    return memory;
  }

  public void setMemory(int memory) {
    this.memory = memory;
  }

  public int getHdd() {
    return hdd;
  }

  public void setHdd(int hdd) {
    this.hdd = hdd;
  }

  public int getBandwidth() {
    return bandwidth;
  }

  public void setBandwidth(int bandwidth) {
    this.bandwidth = bandwidth;
  }

  public int getNodes() {
    return nodes;
  }

  public void setNodes(int nodes) {
    this.nodes = nodes;
  }

  public boolean isActive() {
    return isActive;
  }

  public void setActive(boolean active) {
    isActive = active;
  }

  public List<Cloud> getClouds() {
    return clouds;
  }

  public void setClouds(List<Cloud> clouds) {
    this.clouds = clouds;
  }

  public boolean isDefault() {
    return isDefault;
  }

  public void setDefault(boolean aDefault) {
    isDefault = aDefault;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public ArrayList<VM> getVmList() {
    return vmList;
  }








  public boolean createVmOnCluster(VM vm) {
    if(!(TS.allocateCoresToVm(vm))){
      return false;
    }

    if(!(TS.allocateMemoryToVm(vm))) {
      TS.deallocateCoresForVm(vm);
      return false;
    }

    if(!(TS.allocateBWToVm(vm))) {
      TS.deallocateCoresForVm(vm);
      TS.deallocateMemForVm(vm);
      return false;
    }

    if(!(TS.allocateHddToVm(vm))) {
      TS.deallocateCoresForVm(vm);
      TS.deallocateMemForVm(vm);
      TS.deallocateBwForVm(vm);
      return false;
    }

    getVmList().add(vm);
    vm.setCluster(this);
    return true;
  }

}
