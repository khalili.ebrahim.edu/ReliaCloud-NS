package models;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Indexed;
import org.mongodb.morphia.annotations.Reference;
import org.mongodb.morphia.query.UpdateOperations;
import org.mongodb.morphia.utils.IndexDirection;

import controllers.MorphiaObject;
import play.data.validation.Constraints;

/**
 * Created with IntelliJ IDEA. User: Brett Date: 1/9/13 Time: 1:55 PM
 */
@Entity
public class Simulation {
  @Id
  private ObjectId id;

  @Indexed(value = IndexDirection.DESC)
  private Date date;

  private String name;

  @Constraints.Required
  private int numVMs;

  @Constraints.Required
  private int numSims;
  private int numCompleted;

  @Reference
  private User user;

  @Reference
  private Cloud cloud;

  @Reference
  private VMGroup vmGroup;

  @Embedded
  private ArrayList<VMProbabilityMap> probabilityMap = new ArrayList<>();

  @Embedded
  private ArrayList<SimulationResult> results = new ArrayList<>(numSims);

  private String simType;
  private String vmAllocationType;
  private String vmDistributionType;

  private String convergenceType;

  // new variables
  private String oversubType;

  private double runTime;
  private boolean isValid;
  private boolean isActive;
  private double desiredAvail;
  private double avgRel;
  private double stdRel;

  private List<Integer> hist = new ArrayList<>(101); // histogram counts

  public static Simulation findById(String id) {
    return MorphiaObject.ds.find(Simulation.class).field("_id")
            .equal(new ObjectId(id)).get();
  }

  public static void create(Simulation s) {
    MorphiaObject.ds.save(s);
  }

  public static void update(Simulation s) {
    MorphiaObject.ds.save(s);
  }

  // Add result to the list and increment numberCompleted
  public static void addResult(Simulation s, SimulationResult sr) {
    UpdateOperations<Simulation> updateOperations =
            MorphiaObject.ds.createUpdateOperations(Simulation.class)
                    .add("results", sr).inc("numCompleted");
    MorphiaObject.ds.update(MorphiaObject.ds.createQuery(Simulation.class)
            .field("_id").equal(s.getId()), updateOperations);
  }

  // Add result to the list, increment numberCompleted, and set runTime
  public static void addResult(Simulation s, SimulationResult sr,
                               double runTime, double avgRel, double stdRel, List<Integer> histCounts) {
    UpdateOperations<Simulation> updateOperations = MorphiaObject.ds
            .createUpdateOperations(Simulation.class).add("results", sr)
            .inc("numCompleted").set("runTime", runTime).set("avgRel", avgRel)
            .set("stdRel", stdRel).set("hist", histCounts);

    MorphiaObject.ds.update(MorphiaObject.ds.createQuery(Simulation.class)
            .field("_id").equal(s.getId()), updateOperations);
  }

  public static void addResult(Simulation s, SimulationResult sr,
                               double runTime, double avgRel, double stdRel) {
    UpdateOperations<Simulation> updateOperations =
            MorphiaObject.ds.createUpdateOperations(Simulation.class)
                    .add("results", sr).inc("numCompleted").set("runTime", runTime)
                    .set("avgRel", avgRel).set("stdRel", stdRel);

    MorphiaObject.ds.update(MorphiaObject.ds.createQuery(Simulation.class)
            .field("_id").equal(s.getId()), updateOperations);
  }

  public ObjectId getId() {
    return id;
  }

  public void setId(ObjectId id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getNumVMs() {
    return numVMs;
  }

  public void setNumVMs(int numVMs) {
    this.numVMs = numVMs;
  }

  public Cloud getCloud() {
    return cloud;
  }

  public void setCloud(Cloud cloud) {
    this.cloud = cloud;
  }

  public VMGroup getVmGroup() {
    return vmGroup;
  }

  public void setVmGroup(VMGroup vmGroup) {
    this.vmGroup = vmGroup;
  }

  public String getSimType() {
    return simType;
  }

  public void setSimType(String simType) {
    this.simType = simType;
  }

  public boolean isValid() {
    return isValid;
  }

  public void setValid(boolean valid) {
    isValid = valid;
  }

  public boolean isActive() {
    return isActive;
  }

  public void setActive(boolean active) {
    isActive = active;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public int getNumSims() {
    return numSims;
  }

  public void setNumSims(int numSims) {
    this.numSims = numSims;
  }

  public double getDesiredAvail() {
    return desiredAvail;
  }

  public void setDesiredAvail(double desiredAvail) {
    this.desiredAvail = desiredAvail;
  }

  public String getVmAllocationType() {
    return vmAllocationType;
  }
  public String getVmDistributionType() {
    return vmDistributionType;
  }

  public void setVmAllocationType(String vmAllocationType)      { this.vmAllocationType = vmAllocationType; }
  public void setVmDistributionType(String vmDistributionType)  { this.vmDistributionType = vmDistributionType; }

  public String getConvergenceType() {
    return convergenceType;
  }
  public void setConvergenceType(String convergenceType) {
    this.convergenceType = convergenceType;
  }

  public int getNumCompleted() {
    return numCompleted;
  }

  public void setNumCompleted(int numCompleted) {
    this.numCompleted = numCompleted;
  }

  public ArrayList<VMProbabilityMap> getProbabilityList() {
    return probabilityMap;
  }

  public void setProbabilityList(ArrayList<VMProbabilityMap> probabilityMap) {
    this.probabilityMap = probabilityMap;
  }

  public HashMap<String, Double> getProbabilityMap() {
    HashMap<String, Double> probs = new HashMap<>();

    for (VMProbabilityMap map : probabilityMap) {
      probs.put(map.getVm().getId().toString(), map.getProbability());
    }

    return probs;
  }

  public ArrayList<SimulationResult> getResults() {
    return results;
  }

  public void setResults(ArrayList<SimulationResult> results) {
    this.results = results;
  }

  public Date getDate() {
    return date;
  }

  public void setDate(Date date) {
    this.date = date;
  }

  public double getRunTime() {
    return runTime;
  }

  public void setRunTime(double runTime) {
    this.runTime = runTime;
  }

  public double getAvgRel() {
    return avgRel;
  }

  public void setAvgRel(double avgRel) {
    this.avgRel = avgRel;
  }

  public double getStdRel() {
    return stdRel;
  }

  public void setStdRel(double stdRel) {
    this.stdRel = stdRel;
  }

  public List<Integer> getHist() {
    return hist;
  }

  public void setHist(List<Integer> hist) {
    this.hist = hist;
  }

  public static void markInactive(String id) {
    Simulation sim = findById(id);
    sim.setActive(false);

    update(sim);
  }

  public static List<Simulation> page(String d, int pageSize, String sortBy,
                                      String order, String filter) {
    User user = User.findCurrentUser();

    String dateNoDashes = d.replaceAll("-", " ");

    DateFormat df = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");
    Date date = new Date();
    if (!d.equals("") && !d.equals(null) && d.length() > 25) {
      try {
        date = df.parse(dateNoDashes);
      } catch (ParseException e) {
        e.printStackTrace();
      }
    }

    if (filter.equals("")) {
      return MorphiaObject.ds.find(Simulation.class)
              .retrievedFields(false, "results", "probabilityMap").field("user")
              .equal(user).order("-date").filter("date <", date).limit(pageSize)
              .asList();
    } else {
      Pattern nameFilter =
              Pattern.compile(filter.trim(), Pattern.CASE_INSENSITIVE);

      return MorphiaObject.ds.find(Simulation.class)
              .retrievedFields(false, "results", "probabilityMap").field("user")
              .equal(user).filter("name", nameFilter).order("-date")
              .filter("date <", date).limit(pageSize).asList();
    }
  }

  public static Simulation getSimNameAndType(String simId) {
    return MorphiaObject.ds.find(Simulation.class).field("_id")
            .equal(new ObjectId(simId)).retrievedFields(true, "name", "simType")
            .get();
  }

  public static Simulation getSimOverview(String simId) {
    return MorphiaObject.ds.find(Simulation.class).field("_id")
            .equal(new ObjectId(simId)).retrievedFields(true, "name", "simType",
                    "vmAllocationType", "vmDistributionType", "oversubType", "numSims", "avgRel", "stdRel", "hist")
            .get();
  }

  @Override
  public String toString() {
    return "Simulation{" + "id=" + id + ", name='" + name + '\'' + ", numVMs="
            + numVMs + ", numSims=" + numSims + ", numCompleted=" + numCompleted
            + ", user=" + user + ", cloud=" + cloud + ", vmGroup=" + vmGroup
            + ", probabilityMap=" + probabilityMap + ", results=" + results
            + ", simType='" + simType + '\'' + ", vmAllocationType='" + vmAllocationType
            + ", vmDistributionType='" + vmDistributionType
            + '\'' + ", convergenceType='" + convergenceType + '\'' + ", isValid="
            + isValid + ", isActive=" + isActive + ", desiredAvail=" + desiredAvail
            + '}';
  }

  public String getOversubType() {
    return oversubType;
  }

  public void setOversubType(String oversubType) {
    this.oversubType = oversubType;
  }
}
