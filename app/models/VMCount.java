package models;


import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Reference;

/**
 * Created with IntelliJ IDEA. User: Brett Date: 3/18/13 Time: 12:31 PM To
 * change this template use File | Settings | File Templates.
 */
@Embedded
public class VMCount {
  @Reference
  private VM vm;
  private int count;

  public VMCount() {}

  public VMCount(VM vm, int count) {
    this.vm = vm;
    this.count = count;
  }

  public VM getVm() {
    return vm;
  }

  public int getCount() {
    return count;
  }
}
