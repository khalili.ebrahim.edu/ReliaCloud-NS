package models;

import com.sun.org.apache.bcel.internal.generic.MONITORENTER;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

import controllers.Application;
import controllers.MorphiaObject;
import play.libs.Scala;
import scala.Option;
import securesocial.core.AuthenticationMethod;
import securesocial.core.IdentityId;
import securesocial.core.OAuth1Info;
import securesocial.core.OAuth2Info;
import securesocial.core.PasswordInfo;
import securesocial.core.SocialUser;

/**
 * Created with IntelliJ IDEA. User: Brett Date: 1/23/13 Time: 2:43 PM
 */

@Entity("users")
public class User {

  @Id
  private ObjectId ident;
  private String userId;
  private String first;
  private String last;
  private String email;
  private String authMethod;
  private String token;
  private String secret;
  private String accessToken;
  private String tokenType;
  private String refreshToken;
  private String hasher;
  private String password;
  private String salt;
  private String provider;
  private boolean isActive;

  public User() {}

  // Converts from the user info stored in DB to object used for authentication
  public static SocialUser findByUserId(String id) {
    if (MorphiaObject.ds != null) {
      DBCollection coll = MorphiaObject.ds.getDB().getCollection("users");
      BasicDBObject query = new BasicDBObject("userId", id);
      DBCursor cursor = coll.find(query);

      DBObject userObj;
      if (cursor.hasNext()) {
        userObj = cursor.next();
      } else
        return null;

      String userId = (String) userObj.get("userId");
      String first = (String) userObj.get("first");
      String last = (String) userObj.get("last");

      Option<String> email = Scala.Option((String) userObj.get("email"));
      Option<String> tokenType =
          Scala.Option((String) userObj.get("tokenType"));
      Option<String> refreshToken =
          Scala.Option((String) userObj.get("refreshToken"));
      Option<String> salt = Scala.Option((String) userObj.get("salt"));

      String token = (String) userObj.get("token");
      String secret = (String) userObj.get("secret");
      String access = (String) userObj.get("accessToken");
      String hasher = (String) userObj.get("hasher");
      String password = (String) userObj.get("password");
      String provider = (String) userObj.get("provider");
      String authMethod = (String) userObj.get("authMethod");

      Option<OAuth1Info> oa1 = Scala.Option(new OAuth1Info(token, secret));
      Option<OAuth2Info> oa2 =
          Scala.Option(new OAuth2Info(access, tokenType, null, refreshToken));
      Option<PasswordInfo> pInfo =
          Scala.Option(new PasswordInfo(hasher, password, salt));

      return new SocialUser(new IdentityId(userId, provider), first, last,
          first + " " + last, email, null, new AuthenticationMethod(authMethod),
          oa1, oa2, pInfo);
    }

    // else
    return null;
  }

  public static void create(User u) {
    MorphiaObject.ds.save(u);
  }

  public static User findCurrentUser() {
    return MorphiaObject.ds.find(User.class, "userId", Application.getUserId())
        .get();
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getFirst() {
    return first;
  }

  public void setFirst(String first) {
    this.first = first;
  }

  public String getLast() {
    return last;
  }

  public void setLast(String last) {
    this.last = last;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getAuthMethod() {
    return authMethod;
  }

  public void setAuthMethod(String authMethod) {
    this.authMethod = authMethod;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public String getSecret() {
    return secret;
  }

  public void setSecret(String secret) {
    this.secret = secret;
  }

  public String getAccessToken() {
    return accessToken;
  }

  public void setAccessToken(String accessToken) {
    this.accessToken = accessToken;
  }

  public String getTokenType() {
    return tokenType;
  }

  public void setTokenType(String tokenType) {
    this.tokenType = tokenType;
  }

  public String getRefreshToken() {
    return refreshToken;
  }

  public void setRefreshToken(String refreshToken) {
    this.refreshToken = refreshToken;
  }

  public String getHasher() {
    return hasher;
  }

  public void setHasher(String hasher) {
    this.hasher = hasher;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getSalt() {
    return salt;
  }

  public void setSalt(String salt) {
    this.salt = salt;
  }

  public String getProvider() {
    return provider;
  }

  public void setProvider(String provider) {
    this.provider = provider;
  }

  public boolean isActive() {
    return isActive;
  }

  public void setActive(boolean active) {
    isActive = active;
  }

  @Override
  public String toString() {
    return "User{" + "fullName='" + first + " " + last + '\'' + ", email="
        + email + ", authMethod='" + authMethod + '\'' + ", provider='"
        + provider + '\'' + '}';
  }
}
