package Allocation;

import models.*;
import simulation.data.SimSetupData;
import java.util.*;


public class VmAllocation {
    static int index = -1;


    public static SimulationResult allocateVms(SimSetupData ssd) {
        String allocationType = ssd.getVmAllocationType();
        SimulationResult sr = null;

        if (allocationType.equals("Original"))         { sr = original(ssd); }
        else if (allocationType.equals("FCFS"))        { sr = firstComeFirstServe(ssd); }
        else if (allocationType.equals("RoundRobin"))  { sr = RoundRobin(ssd); }

        return sr;
    }

    public static SimulationResult original(SimSetupData ssd)  {
        boolean downsample = true;
        final int NUMBER_OF_PTS = 300;

        long startTime = System.nanoTime();
        double locp = 1.0;
        double sigma = 1.0;
        int totalFails = 0;
        int totalSamples = 0;
        double vLocp;


        List<Long> cpuAvail = new ArrayList<>(2000000);
        List<Long> memAvail = new ArrayList<>(2000000);
        List<Long> hddAvail = new ArrayList<>(2000000);
        List<Long> bandAvail = new ArrayList<>(2000000);

        List<Double> locsList = new ArrayList<>(2000000);
        List<Double> vLocsList = new ArrayList<>(2000000);

        int pFails = 0;
        int mFails = 0;
        int hFails = 0;
        int bFails = 0;

        long hAvail;
        long pAvail;
        long bAvail;
        long mAvail;

        int pNeeded = ssd.getpNeeded();
        int mNeeded = ssd.getmNeeded();
        int hNeeded = ssd.gethNeeded();
        int bNeeded = ssd.getbNeeded();

        // retrieve simulation for saving SimulationResult
        final Simulation sim = ssd.getSim();

        System.out.println(sim.getId() + " simulation started");
        int countTrials = 0;
        //System.out.println("Trial No : " + countTrials);
        countTrials++;

        int counter =0 ;
        while (true) {

            counter++;
            //System.out.println("Sample No : "+ counter);

            hAvail = 0;
            pAvail = 0;
            bAvail = 0;
            mAvail = 0;

            boolean[] currState;

            for (final Cluster cluster : sim.getCloud().getClusters()) {
                for (int j = 0; j < cluster.getNodes(); j++) {
                    currState = cluster.getState();

                    if (currState[0]){
                        pAvail += cluster.getCores();}
                    if (currState[1])
                        mAvail += cluster.getMemory();
                    if (currState[2])
                        hAvail += cluster.getHdd();
                    if (currState[3])
                        bAvail += cluster.getBandwidth();
                }
            }
            cpuAvail.add(pAvail);
            memAvail.add(mAvail);
            hddAvail.add(hAvail);
            bandAvail.add(bAvail);

            if (!(pAvail >= pNeeded && mAvail >= mNeeded && hAvail >= hNeeded
                    && bAvail >= bNeeded)) {



                if (pAvail < pNeeded)
                    pFails++;
                if (mAvail < mNeeded)
                    mFails++;
                if (hAvail < hNeeded)
                    hFails++;
                if (bAvail < bNeeded)
                    bFails++;

                totalFails++;
                System.out.println( "total Fail at this sample: " + totalFails);
            }


            totalSamples++;

            // calculate LOCS and vLocs and add to list
            locp = ((double) totalFails / totalSamples);
            vLocp = (1.0 / totalSamples) * locp * (1 - locp);

            locsList.add(locp);
            vLocsList.add(vLocp);

            if (locp == 0)
                sigma = 1.0;
            else
                sigma = (Math.sqrt(vLocp) / locp);

            if ((totalSamples > 10 && sigma < 0.08)
                    || (totalSamples >= 20000 && locp < 0.000001 && sigma == 1))

                break;
        }

        long duration = System.nanoTime() - startTime;
        double seconds = (duration / 1000000.0) / 1000.0;
        double runTime = (Double.valueOf(seconds));

        SimulationResult sr;
        if (downsample) {
            List<Double> locpListDS = new ArrayList<>(NUMBER_OF_PTS);
            List<Double> vLocpListDS = new ArrayList<>(NUMBER_OF_PTS);

            List<Long> cpuAvailDS = new ArrayList<>(NUMBER_OF_PTS);
            List<Long> memAvailDS = new ArrayList<>(NUMBER_OF_PTS);
            List<Long> hddAvailDS = new ArrayList<>(NUMBER_OF_PTS);
            List<Long> bandAvailDS = new ArrayList<>(NUMBER_OF_PTS);

            int sampNum = 0;
            while (sampNum < totalSamples) {
                locpListDS.add(locsList.get(sampNum));
                vLocpListDS.add(vLocsList.get(sampNum));
                cpuAvailDS.add(cpuAvail.get(sampNum));
                memAvailDS.add(memAvail.get(sampNum));
                hddAvailDS.add(hddAvail.get(sampNum));
                bandAvailDS.add(bandAvail.get(sampNum));


                if (totalSamples > 1000)
                    sampNum += (totalSamples / NUMBER_OF_PTS);
                else
                    sampNum++;
            }

            sr = new SimulationResult(pFails, mFails, hFails, bFails, totalFails,
                    totalSamples, locp, sigma, true, true, runTime, pNeeded, mNeeded,
                    hNeeded, bNeeded, ssd.getCounts(), cpuAvailDS, memAvailDS, hddAvailDS,
                    bandAvailDS, locpListDS, vLocpListDS);
        } else { // legacy now that all results are stored in PostgreSQL
            sr = new SimulationResult(pFails, mFails, hFails, bFails, totalFails,
                    totalSamples, locp, sigma, true, true, runTime, pNeeded, mNeeded,
                    hNeeded, bNeeded, ssd.getCounts(), cpuAvail, memAvail, hddAvail,
                    bandAvail, locsList, vLocsList);
        }

        return  sr;
    }


    public static SimulationResult firstComeFirstServe(SimSetupData ssd) {
        // TODO: Add code to run simualtino based on first come first serve simulation here

        boolean downsample = true;
        final int NUMBER_OF_PTS = 300;

        long startTime = System.nanoTime();
        double locp = 1.0;
        double sigma = 1.0;
        int totalFails = 0;
        int totalSamples = 0;
        double vLocp;


        List<Long> cpuAvail = new ArrayList<>(2000000);
        List<Long> memAvail = new ArrayList<>(2000000);
        List<Long> hddAvail = new ArrayList<>(2000000);
        List<Long> bandAvail = new ArrayList<>(2000000);

        List<Double> locsList = new ArrayList<>(2000000);
        List<Double> vLocsList = new ArrayList<>(2000000);

        final Simulation sim = ssd.getSim();


        System.out.println(sim.getId() + " simulation started");


        List<Cluster> clusterList = new ArrayList<>();
        while (true){

            clusterList.clear();
            boolean[] currState;
            int pAvail =0 , mAvail = 0 , hAvail = 0 , bAvail=0 ;
            for (final Cluster cluster : sim.getCloud().getClusters()) {
                for (int j = 0; j < cluster.getNodes(); j++) {
                    currState = cluster.getState();

                    if (currState[0])
                        pAvail = cluster.getCores();
                    if (currState[1])
                        mAvail = cluster.getMemory();
                    if (currState[2])
                        hAvail = cluster.getHdd();
                    if (currState[3])
                        bAvail = cluster.getBandwidth();

                    cluster.setCores(pAvail);
                    cluster.setMemory(mAvail);
                    cluster.setHdd(hAvail);
                    cluster.setBandwidth(bAvail);
                    clusterList.add(cluster);

                }
            }


            int countFails = 0;

            List<VM> vm_list = ssd.getVms();

            for (int i =0 ; i< vm_list.size() ; i++){

                VM vm  = vm_list.get(i);
                // store current vm resource in variables
                int coreRequested = vm.getCores();
                int bwRequested = vm.getBandwidth();
                int memRequested = vm.getMemory();
                int hddRequested = vm.getHdd();

                int index_no = ChooseMoreFreeCluster(clusterList);

                if(index != -1) {
                    int coreAvail = clusterList.get(index_no).getCores();
                    int bwAvail   = clusterList.get(index_no).getBandwidth();
                    int memiAvail = clusterList.get(index_no).getMemory();
                    int hddiAvail = clusterList.get(index_no).getHdd();

                    if (coreAvail >= coreRequested && memiAvail >= memRequested &&hddiAvail>= hddRequested && bwAvail >= bwRequested) {

                        // if this vm allocated th````en available resource will go down
                        clusterList.get(index_no).setCores(coreAvail - coreRequested);
                        clusterList.get(index_no).setMemory(memiAvail - memRequested);
                        clusterList.get(index_no).setBandwidth(bwAvail - bwRequested);
                        clusterList.get(index_no).setHdd(hddiAvail- hddRequested);

                    } else {
                        // here single vm will fails then we increase counter of individual fails
                        countFails++;
                    }
                }else{
                    countFails++;
                }

            }// end vmList Loop

            if (countFails > 0) { // not all vms are allocated
                totalFails++;
            }
            totalSamples++;

            // calculate LOCS and vLocs and add to list
            locp = ((double) totalFails / totalSamples);
            vLocp = (1.0 / totalSamples) * locp * (1 - locp);

            locsList.add(locp);
            vLocsList.add(vLocp);

            if (locp == 0)  { sigma = 1.0;}
            else            { sigma = (Math.sqrt(vLocp) / locp);}



            if ((totalSamples > 10 && sigma < 0.08) ||
                    (totalSamples >= 20000 && locp < 0.000001 && sigma == 1))
                break;
        }


        long duration = System.nanoTime() - startTime;
        double seconds = (duration / 1000000.0) / 1000.0;
        double runTime = (Double.valueOf(seconds));

        SimulationResult sr;
        if (downsample) {
            List<Double> locpListDS = new ArrayList<>(NUMBER_OF_PTS);
            List<Double> vLocpListDS = new ArrayList<>(NUMBER_OF_PTS);

            int sampNum = 0;
            while (sampNum < totalSamples) {
                locpListDS.add(locsList.get(sampNum));
                vLocpListDS.add(vLocsList.get(sampNum));

                if (totalSamples > 1000)
                    sampNum += (totalSamples / NUMBER_OF_PTS);
                else
                    sampNum++;
            }

            sr = new SimulationResult(totalFails,totalSamples, locp, sigma, true, true, runTime, locpListDS, vLocpListDS);
        } else { // legacy now that all results are stored in PostgreSQL
            sr = new SimulationResult(totalFails,
                    totalSamples, locp, sigma, true, true, runTime,  locsList, vLocsList);


        }
        return sr;
    }


    /**************************************************************************************************************/

    public static SimulationResult RoundRobin(SimSetupData ssd) {
        // TODO: Add code to run simualtino based on first come first serve simulation here
/*
        boolean downsample = true;
        final int NUMBER_OF_PTS = 300;
        boolean addResource =false;
        int n = 1;

        long startTime = System.nanoTime();
        double locp = 1.0;
        double sigma = 1.0;
        int totalFails = 0;
        int totalSamples = 0;
        double vLocp;


        List<Double> locsList = new ArrayList<>(2000000);
        List<Double> vLocsList = new ArrayList<>(2000000);


        List<VM> vm_list = ssd.getVms();
        List<Cluster> clusterList= ssd.getClusters();




        // retrieve simulation for saving SimulationResult
        Simulation sim = ssd.getSim();
        System.out.println(sim.getId() + " simulation started");

        //for(int i= 1 ; i <= 4; i++){
        //  Cluster newCluster = additionCluster(sim);
        //clusterList.add(newCluster);

        // }




        sampling:
        while (true) {

            LinkedList<VM> vm_linkedlist = new LinkedList<>();
            for (VM vm:vm_list)
                vm_linkedlist.add(vm);


            int countFails=0;
            for (int i=0;i<vm_linkedlist.size();i++){
                VM vm = vm_linkedlist.get(i);
                int coreRequested = vm.getCores();
                int bwRequested = vm.getBandwidth();
                int memRequested = vm.getMemory();

                Cluster clust = ChooseMoreFreeCluster(clusterList);

                if(clust != null) {
                    if (clust.getCores() >= coreRequested && clust.getMemory() >= memRequested &&
                            clust.getBandwidth() >= bwRequested) {
                        // System.out.println("The VM ("+i+") allocate to cluster ("+clusterNo+")");

                        clust.setCores(clust.getCores() - coreRequested);
                        clust.setMemory(clust.getMemory() - memRequested);
                        clust.setBandwidth(clust.getBandwidth() - bwRequested);
                        vm_linkedlist.remove(vm_linkedlist.get(i));


                    } else {
                        vm_linkedlist.addLast(vm);
                        vm_linkedlist.removeFirst();
                        countFails++;

                    }

                }else{
                    vm_linkedlist.addLast(vm);
                    vm_linkedlist.removeFirst();
                    countFails++;

                }


            } // end vm list

            if(countFails >0)
                totalFails++;


            totalSamples++;

            // calculate LOCS and vLocs and add to list
            locp = ((double) totalFails / totalSamples);
            vLocp = (1.0 / totalSamples) * locp * (1 - locp);

            locsList.add(locp);
            vLocsList.add(vLocp);

            if (locp == 0
                    )
                sigma = 1.0;
            else
                sigma = (Math.sqrt(vLocp) / locp);



            if ((totalSamples > 10 && sigma < 0.08)
                    || (totalSamples >= 20000 && locp < 0.000001 && sigma == 1))

                break;

        }


        long duration = System.nanoTime() - startTime;
        double seconds = (duration / 1000000.0) / 1000.0;
        double runTime = (Double.valueOf(seconds));

        SimulationResult sr;
        if (downsample) {
            List<Double> locpListDS = new ArrayList<>(NUMBER_OF_PTS);
            List<Double> vLocpListDS = new ArrayList<>(NUMBER_OF_PTS);



            int sampNum = 0;
            while (sampNum < totalSamples) {
                locpListDS.add(locsList.get(sampNum));
                vLocpListDS.add(vLocsList.get(sampNum));

                if (totalSamples > 1000)
                    sampNum += (totalSamples / NUMBER_OF_PTS);
                else
                    sampNum++;
            }

            sr = new SimulationResult(totalFails,totalSamples, locp, sigma, true, true, runTime, locpListDS, vLocpListDS);
        } else { // legacy now that all results are stored in PostgreSQL
            sr = new SimulationResult(totalFails,
                    totalSamples, locp, sigma, true, true, runTime,  locsList, vLocsList);


        }
        return sr;*/
        return null;
    }





    /**************************************************************************************************************/



    public static int ChooseMoreFreeCluster(List<Cluster> clustList) {

        int max = Integer.MIN_VALUE;

        for (int i = 0; i < clustList.size(); i++) {
            if (clustList.get(i).getCores() > max) {
                max = clustList.get(i).getCores();
                index = i;
            }
            if(clustList.get(i).getCores()==0)
                index = -1;
        }

        return index;
    }
    /////////////////////////////////////////////////////////////////////////
    public static Cluster ChooseNode(List<Cluster> clustList) {

        int max = Integer.MIN_VALUE;
        int index = -1;
        int serverNo = 0;

        for (int i = 0; i < clustList.size(); i++) {
            for (int j= 0 ; j< clustList.get(i).getNodes() ; j++) {
                if (clustList.get(i).getCores() > max) {
                    max = clustList.get(i).getCores();
                    index = i;
                }
            }
        }


        return clustList.get(index);
    }
    /////////////////////////////////////////////////////////////////////
    public static Cluster addResourceToVm(VM vm) {
        Cluster c = new Cluster();
        c.setCores(vm.getCores());
        c.setMemory(vm.getMemory());
        c.setHdd(vm.getHdd());
        c.setBandwidth(vm.getBandwidth());
        c.setNodes(1);

        return c;
    }

    ////////////////////////////////////////////////////////////////
    public static Cluster additionCluster(Simulation s) {


        int cpuNeed = 0;
        int memNeed = 0;
        int hddNeed = 0;
        int bandNeed = 0;

        for (VMProbabilityMap vmpm : s.getProbabilityList()) {
            if (vmpm.getProbability() != 0) {
                VM curr = vmpm.getVm();

                cpuNeed += curr.getCores() * vmpm.getProbability();
                memNeed += curr.getMemory() * vmpm.getProbability();
                hddNeed += curr.getHdd() * vmpm.getProbability();
                bandNeed += curr.getBandwidth() * vmpm.getProbability();
            }
        }

        int cpuReq= 0;
        int memReq = 0;
        int hddReq = 0;
        int bandReq = 0;

        cpuReq = (s.getNumVMs() * cpuNeed);
        memReq =(s.getNumVMs() * memNeed);
        hddReq = (s.getNumVMs() * hddNeed);
        bandReq =(s.getNumVMs() * bandNeed);


        Cluster c = new Cluster();
        c.setCores(cpuReq);
        c.setMemory(memReq);
        c.setHdd(hddReq);
        c.setBandwidth(bandReq);
        c.setNodes(1);


        c.toString();
        return c;
    }

}




