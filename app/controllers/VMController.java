package controllers;

import static play.data.Form.form;

import org.bson.types.ObjectId;

import models.User;
import models.VM;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import securesocial.core.java.SecureSocial;
import views.html.vmCreateForm;
import views.html.vmEditForm;
import views.html.vmList;


/**
 * Created with IntelliJ IDEA. User: Brett Date: 1/9/13 Time: 4:59 PM
 */

public class VMController extends Controller {
  private final static Form<VM> vmForm = form(VM.class);

  @SecureSocial.SecuredAction
  public static Result create() {
    return ok(vmCreateForm.render(vmForm));
  }

  /**
   * Handle the 'new VM form' submission
   */
  @SecureSocial.SecuredAction
  public static Result save() {
    Form<VM> vmForm = form(VM.class).bindFromRequest();
    if (vmForm.hasErrors()) {
      return badRequest(vmCreateForm.render(vmForm));
    }

    vmForm.get().setActive(true); // on creation set to active
    vmForm.get().setDefault(false); // this VM is tied to a specific user
    vmForm.get().setUser(User.findCurrentUser());
    VM.create(vmForm.get());

    flash("success", "VM " + vmForm.get().getName() + " has been created");
    return redirect(routes.VMController.list());
  }

  /**
   * Handle VM deletion
   */
  @SecureSocial.SecuredAction
  public static Result delete(String id) {
    VM.delete(id);
    flash("success", "VM has been deleted");
    return redirect(routes.VMController.list());
  }

  @SecureSocial.SecuredAction
  public static Result markInactive(String id) {
    VM.markInactive(id);
    return redirect(routes.VMController.list());
  }

  @SecureSocial.SecuredAction
  public static Result markActive(String id) {
    VM.markActive(id);
    return redirect(routes.VMController.list());
  }

  /**
   * Display the 'edit form' of a existing VM.
   * 
   * @param id Id of the VM to edit technically this is a 'copy' form.
   */
  @SecureSocial.SecuredAction
  public static Result edit(String id) {
    Form<VM> vmForm = form(VM.class).fill(VM.findById(id));
    return ok(vmEditForm.render(id, vmForm));
  }

  /**
   * Handle the 'edit form' submission
   * 
   * @param id Id of the VM to edit
   */
  @SecureSocial.SecuredAction
  public static Result update(String id) {
    Form<VM> vmForm = form(VM.class).bindFromRequest();
    if (vmForm.hasErrors()) {
      return badRequest(vmEditForm.render(id, vmForm));
    }

    vmForm.get().setId(new ObjectId()); // create a new ID
    vmForm.get().setActive(true);
    vmForm.get().setDefault(false);
    VM.update(vmForm.get());

    flash("success", "VM " + vmForm.get().getName() + " has been updated");
    return redirect(routes.VMController.list());
  }

  @SecureSocial.SecuredAction
  public static Result list() {
    return ok(vmList.render(VM.findActive(), VM.findHidden()));
  }
}
