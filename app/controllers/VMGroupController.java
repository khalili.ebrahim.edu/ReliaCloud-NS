package controllers;

import static play.data.Form.form;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.bson.types.ObjectId;

import models.User;
import models.VM;
import models.VMGroup;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import securesocial.core.java.SecureSocial;
import views.html.vmGroupCreateForm;
import views.html.vmGroupEditForm;
import views.html.vmGroupList;
import views.html.vmGroupViewForm;

/**
 * Created with IntelliJ IDEA. User: Brett Date: 1/11/13 Time: 9:10 AM
 */

public class VMGroupController extends Controller {
  private final static Form<VMGroup> groupForm = form(VMGroup.class);

  @SecureSocial.SecuredAction
  public static Result create() {
    return ok(vmGroupCreateForm.render(groupForm, VM.findActive()));
  }

  /**
   * Handle the 'new VMGroup form' submission
   */
  @SecureSocial.SecuredAction
  public static Result save() {
    Form<VMGroup> groupForm = form(VMGroup.class).bindFromRequest();
    if (groupForm.hasErrors()) {
      return badRequest(vmGroupCreateForm.render(groupForm, VM.findActive()));
    }

    // retrieve request keys in order to determine which VMs were selected for
    // the group
    Set<String> keys = groupForm.bindFromRequest().data().keySet();

    groupForm.get().setVms(generateVmList(keys)); // pull in the newly added VMs
    groupForm.get().setActive(true); // on creation set active to true
    groupForm.get().setDefault(false); // this VM is tied to a specific user
    groupForm.get().setUser(User.findCurrentUser());
    VMGroup.create(groupForm.get());

    flash("success",
        "VM Group" + groupForm.get().getName() + " has been created");
    return redirect(routes.VMGroupController.list());
  }

  @SecureSocial.SecuredAction
  public static Result list() {
    return ok(vmGroupList.render(VMGroup.findActive(), VMGroup.findHidden()));
  }

  /**
   * Display the 'view form' of a existing VMGroup.
   * 
   * @param id Id of the VM to view
   */
  @SecureSocial.SecuredAction
  public static Result view(String id) {
    return ok(vmGroupViewForm.render(VMGroup.findById(id)));
  }

  /**
   * Display the 'edit form' of a existing VMGroup.
   * 
   * @param id Id of the VM to edit
   */
  @SecureSocial.SecuredAction
  public static Result edit(String id) {
    Form<VMGroup> editGroupForm =
        form(VMGroup.class).fill(VMGroup.findById(id));
    return ok(vmGroupEditForm.render(editGroupForm, VMGroup.findById(id),
        VM.findActive()));
  }

  /**
   * Handle the 'edit form' submission
   * 
   * @param id Id of the VM to edit
   */
  @SecureSocial.SecuredAction
  public static Result update(String id) {
    Form<VMGroup> editGroupForm = form(VMGroup.class).bindFromRequest();

    if (editGroupForm.hasErrors()) {
      return badRequest(vmGroupViewForm.render(VMGroup.findById(id)));
    }

    // retrieve request keys in order to determine which VMs were selected for
    // the group
    Set<String> keys = editGroupForm.bindFromRequest().data().keySet();

    editGroupForm.get().setVms(generateVmList(keys)); // pull in the newly added
                                                      // VMs
    editGroupForm.get().setId(new ObjectId()); // assign a new ObjectId as this
                                               // is a copy
    editGroupForm.get().setActive(true); // renew that this is still active on
                                         // updates
    VMGroup.update(editGroupForm.get());

    flash("success",
        "VM Group" + editGroupForm.get().getName() + " has been updated");
    return redirect(routes.VMGroupController.list());
  }

  /**
   * Handle VM deletion
   */
  @SecureSocial.SecuredAction
  public static Result delete(String id) {
    VMGroup.delete(id);

    flash("success", "VM Group has been deleted");
    return redirect(routes.VMGroupController.list());
  }

  @SecureSocial.SecuredAction
  public static Result markInactive(String id) {
    VMGroup.markInactive(id);
    return redirect(routes.VMGroupController.list());
  }

  @SecureSocial.SecuredAction
  public static Result markActive(String id) {
    VMGroup.markActive(id);
    return redirect(routes.VMGroupController.list());
  }

  private static List<VM> generateVmList(Set<String> keys) {
    List<VM> vms = new ArrayList<>();
    for (String key : keys) {
      if (!(key.equals("name") || key.equals("id"))) {
        try {
          vms.add(VM.findById(key));
        } catch (Exception e) {
          System.out.println(e.toString());
        }
      }
    }

    return vms;
  }
}
