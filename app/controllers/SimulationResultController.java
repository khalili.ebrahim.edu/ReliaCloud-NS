package controllers;

import models.Simulation;
import models.SimulationResult;
import play.mvc.Controller;
import play.mvc.Result;
import securesocial.core.java.SecureSocial;
import views.html.simChildrenList;
import views.html.simChildrenResultsTable;
import views.html.simResults;

/**
 * Created with IntelliJ IDEA. User: Brett Date: 1/17/13 Time: 9:55 PM
 */
public class SimulationResultController extends Controller {

  @SecureSocial.SecuredAction
  public static Result displayResult(String simId, int resId) {
    Simulation sim = Simulation.findById(simId);
    SimulationResult res = sim.getResults().get(resId);

    return ok(simResults.render(sim, res));
  }

  /**
   * Display the paginated list of results
   */
  @SecureSocial.SecuredAction
  public static Result getTableById(String simId, int page, String sortBy,
      String order, String filter) {
    return ok(
        simChildrenResultsTable.render(Simulation.getSimNameAndType(simId),
            SimulationResult.pageBySim(simId, page, 20, sortBy, order, filter),
            sortBy, order, filter));
  }

  @SecureSocial.SecuredAction
  public static Result getChildResults(String simId, int page, String sortBy,
      String order, String filter) {

    return ok(simChildrenList.render(
        SimulationResult.pageBySim(simId, page, 20, sortBy, order, filter),
        sortBy, order, filter, Simulation.getSimOverview(simId)));
  }
}
